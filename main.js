var ourData = [];
var moduleContainer = document.getElementById('moduletable');

window.addEventListener("load", function(){
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://bitbucket.org/24264954/cw2/raw/a52e7a6fa35e81a1cc4c8d3392cb1fa676d91a3c/modules.json');
  ourRequest.onload = function(){
    //console.log(ourRequest.responseText);
    ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    renderHTML(ourData);
  };
  ourRequest.send();
});


function renderHTML(data){
  var htmlString = "";
  console.log(data);
  
  for(i = 0; i < data.length; i++){
    
    htmlString += "<tr>" + "<td>" + data[i].Degree.Name + "</td>";

    htmlString += "<td>" + data[i].Module.Course + "</td>";

    htmlString += "<td>" + data[i].Assessment.Assignment + "</td>";

    htmlString += "<td>" + "<input type='date'>" + "</input>" + "</td>";
    
    htmlString += "<td>" + data[i].Time_slot.Day + "-" + data[i].Time_slot.Time + "-" + data[i].Time_slot.Frequency + "-" + data[i].Time_slot.Room + "</td>";

  }

  moduleContainer.innerHTML=htmlString;

}

function filterDegreeTable() {
  var dropDownValue=document.getElementById("degreeDropdown").value
  if (dropDownValue=="") {
    renderHTML(ourData)
  } else {
  var filterData=ourData.filter(function(module){
    return module.Degree.Name===dropDownValue;
    
  })
  renderHTML(filterData)
  }
}


function filterModuleTable() {
  var dropDownValue=document.getElementById("moduleDropdown").value
  if (dropDownValue=="") {
    renderHTML(ourData)
  } else {
  var filterData=ourData.filter(function(module){
    return module.Module.Course===dropDownValue;
    
  })
  renderHTML(filterData)
  }
}



  /*for(i = 0; i < data.length; i++){
    htmlString += "<p>" + data[i].Name + " is a " + data[i].Course + " has assements "; //".</p>";
    for(ii = 0; ii < data[i].Module.Assignment.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Assignment[ii];
      } else {
        htmlString += " and " + data[i].Module.Assignment[ii];
      }
    }
    htmlString += ' and Learning Outcome ';
    for(ii = 0; ii < data[i].Module.Learning_outcomes.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Learning_outcomes[ii];
      } else {
        htmlString += " and " + data[i].Module.Learning_outcomes[ii];
      }
    }

    htmlString += ' and Volume ';
    for(ii = 0; ii < data[i].Module.Volume.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Volume[ii];
      } else {
        htmlString += " and " + data[i].Module.Volume[ii];
      }
    }

    htmlString += ' and weights ';
    for(ii = 0; ii < data[i].Module.weights.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.weights[ii];
      } else {
        htmlString += " and " + data[i].Module.weights[ii];
      }
    }
    htmlString += '.</p>';
  }
  moduleContainer.insertAdjacentHTML('moduletable', htmlString);*/


