var ourData = [];
var moduleContainer = document.getElementById('modtable');

window.addEventListener("load", function(){
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://bitbucket.org/24264954/cw2/raw/a52e7a6fa35e81a1cc4c8d3392cb1fa676d91a3c/modules.json');
  ourRequest.onload = function(){
    //console.log(ourRequest.responseText);
    ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    renderHTML(ourData);
  };
  ourRequest.send();
});

function renderHTML(data){
  var htmlString = "";
  console.log(data);
  
  for(i = 0; i < data.length; i++){
    
    htmlString += "<tr>" + "<td>" + data[i].Module.id + "</td>";

    htmlString += "<td>" + data[i].Module.Course + "</td>";

    htmlString += "<td>" + data[i].Academic + "</td>";

    htmlString += "<td>" + data[i].Module.Hours + "</td>";

    htmlString += "<td>" + data[i].Module.Learning_outcomes + "</td>";
    
    htmlString += "<td>" + data[i].Module.Credits + "</td>";

  }

  moduleContainer.insertAdjacentHTML('beforeend', htmlString);

}
